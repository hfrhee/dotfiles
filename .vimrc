syntax on

set relativenumber
set nu
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
set autoindent
set smartindent
set showcmd
set hlsearch
set incsearch
set wildmode=longest:full,full
set wildmenu
set encoding=utf-8
set list
  if &listchars ==# 'eol:$'
    set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
  endif
set visualbell
set nowrap
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set scrolloff=3
set colorcolumn=80
highlight ColorColumn ctermbg=0 guibg=lightgrey
set signcolumn=yes
set background=dark
set clipboard=unnamed

" vim-plug
" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'mbbill/undotree'
Plug 'morhetz/gruvbox'
  let g:gruvbox_contrast_dark = 'soft'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'vim-latex/vim-latex'
Plug 'vim-airline/vim-airline'
Plug 'Yggdroot/indentLine', { 'on': 'IndentLinesEnable' }

" Clojure
Plug 'junegunn/rainbow_parentheses.vim'
Plug 'tpope/vim-fireplace'

" Copy into clipboard from everywhere
" not working yet
Plug 'wincent/vim-clipper'

" Initialize plugin system
call plug#end()

colorscheme gruvbox

" ============================================================================
" Leader mappings.
"
let mapleader = ' '
" <Leader><Leader> -- Open last buffer.
nnoremap <Leader><Leader> <C-^>

" ============================================================================
" Mappings
"

" Mappings for fzf.vim
nnoremap <silent> <Leader><Enter>  :Buffers<CR>
nnoremap <silent> <Leader>L        :Lines<CR>

nnoremap <silent> <Leader>u :UndotreeShow<CR>

" Coc
" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" wincent-clipper
nnoremap <leader>y :call system('nc -N localhost 8377', @0)<CR>

" ============================================================================
" Plugin Settings

" Setting for vim-latex
let g:tex_flavor='latex'
let g:Tex_DefaultTargetFormat='pdf'
let g:Tex_CompileRule_dvi = 'latex --src-specials -interaction=nonstopmode $*'
let g:Tex_CompileRule_pdf = 'pdflatex -synctex=1 -interaction=nonstopmode $*'
let g:Tex_ViewRule_dvi	= 'xdvi'
let g:Tex_ViewRule_pdf	= 'zathura'

